import React from "react";
import { TouchableOpacity } from "react-native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

import { Home, Portfolio, Market, Profile } from "../screens";
import { TabIcon } from "../components";
import { COLORS, icons } from "../constants";

import { connect } from "react-redux";
import { setTrade } from "../store/tab/tabActions";

const Tab = createBottomTabNavigator();

const TabBarCustomButton = ({ children, onPress }) => {
  return (
    <TouchableOpacity
      style={{
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
      }}
      onPress={onPress}
    >
      {children}
    </TouchableOpacity>
  );
};
const Tabs = ({ setTrade, isTrade }) => {
  const tradeTabButtonClickHander = () => {
    setTrade(!isTrade);
  };
  return (
    <Tab.Navigator
      tabBarOptions={{
        style: {
          backgroundColor: COLORS.primary,
          borderTopColor: "transparent",
        },
      }}
    >
      <Tab.Screen
        name="Home"
        component={Home}
        options={{
          tabBarOptions: ({ focused }) => {
            if (!isTrade) {
              return (
                <TabIcon focused={focused} icon={icons.trade} isTrade={true} />
              );
            }
          },
        }}
        listeners={{
          tabPress: (e) => {
            if (isTrade) {
              e.preventDefault();
            }
          },
        }}
      />
      <Tab.Screen
        name="Portfolio"
        component={Portfolio}
        options={{
          tabBarOptions: ({ focused }) => {
            if (!isTrade) {
              return <TabIcon focused={focused} icon={icons.portfolio} />;
            }
          },
        }}
        listeners={{
          tabPress: (e) => {
            if (isTrade) {
              e.preventDefault();
            }
          },
        }}
      />
      <Tab.Screen
        name="Trade"
        component={Home}
        options={{
          tabBarOptions: ({ focused }) => {
            return (
              <TabIcon
                label="Trade"
                focused={focused}
                icon={isTrade ? icons.close : icons.trade}
                iconStyle={isTrade ? { with: 15, height: 15 } : null}
                isTrade={true}
              />
            );
          },
          tabBarOptions: (props) => {
            <TabBarCustomButton
              {...props}
              onPress={() => tradeTabButtonClickHander}
            />;
          },
        }}
      />
      <Tab.Screen
        name="Market"
        component={Market}
        options={{
          tabBarOptions: ({ focused }) => {
            if (!isTrade) {
              return <TabIcon focused={focused} icon={icons.market} />;
            }
          },
        }}
        listeners={{
          tabPress: (e) => {
            if (isTrade) {
              e.preventDefault();
            }
          },
        }}
      />
      <Tab.Screen
        name="Profile"
        component={Profile}
        options={{
          tabBarOptions: ({ focused }) => {
            if (!isTrade) {
              return <TabIcon focused={focused} icon={icons.profile} />;
            }
          },
        }}
        listeners={{
          tabPress: (e) => {
            if (isTrade) {
              e.preventDefault();
            }
          },
        }}
      />
    </Tab.Navigator>
  );
};

export default Tabs;
