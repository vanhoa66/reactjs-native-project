import * as tabActionTypes from "./tabActions";

const initialState = {
  isTrade: false,
};

const tabReducer = (state = initialState, action) => {
  switch (action.type) {
    case tabActionTypes.SET_TRADE:
      return {
        ...state,
        isTrade: action.payload.isVisible,
      };
    default:
      return state;
  }
};

export default tabReducer;
