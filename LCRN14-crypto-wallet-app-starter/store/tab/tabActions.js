export const SET_TRADE = "SET_TRADE";

export const setTradeSuccess = (isVisible) => ({
  type: SET_TRADE,
  payload: isVisible,
});

export function setTrade(isVisible) {
  return (dispatch) => {
    setTradeSuccess(isVisible);
  };
}
