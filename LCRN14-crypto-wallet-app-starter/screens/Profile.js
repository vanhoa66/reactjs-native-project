import React, { useSelector, useDispatch, useState } from "react";
import {
  View,
  Text,
  FlatList,
  TouchableOpacity,
  Animated,
  ScrollView,
} from "react-native";
import { MainLayout } from "./MainLayout";
import { getHoldings, getCoinMarket } from "../store/market/marketActions";
import { useFocusEffect } from "@react-navigation/native";

import {
  SIZES,
  COLORS,
  FONTS,
  icons,
  holdings,
  dummyData,
  constants,
} from "../constants";
import { BalanceInfo, TextButton, Chart, HeaderBar } from "../components";

const SectionTitle = ({ title }) => {
  return (
    <View
      style={{
        marginTop: SIZES.padding,
      }}
    >
      <Text
        style={{
          color: COLORS.lightGray3,
          ...FONTS.h4,
        }}
      >
        {title}
      </Text>
    </View>
  );
};
const Setting = ({ title, value, type, onPress }) => {
  if (type == "button") {
    return (
      <TouchableOpacity
        style={{
          flexDirection: "row",
          height: 50,
          alignItems: "center",
        }}
        onPress={onPress}
      >
        <Text
          style={{
            flex: 1,
            color: COLORS.lightGray3,
            ...FONTS.h3,
          }}
        >
          {title}
        </Text>

        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <Text
            style={{
              marginRight: SIZES.radius,
              color: COLORS.lightGray3,
              ...FONTS.h3,
            }}
          >
            {value}
          </Text>

          <Image
            source={icons.rightArrow}
            style={{
              height: 15,
              width: 15,
              tintColor: COLORS.white,
            }}
          />
        </View>
      </TouchableOpacity>
    );
  } else {
    return (
      <View
        style={{
          flexDirection: "row",
          alignItems: "center",
          height: 50,
        }}
      >
        <Text
          style={{
            flex: 1,
            color: COLORS.white,
            ...FONTS.h3,
          }}
        >
          {title}
        </Text>
        <Switch value={value} onValueChange={(value) => onPress(value)} />
      </View>
    );
  }
};

const Profile = () => {
  const [faceId, setFaceId] = useState(true);

  return (
    <MainLayout>
      <View
        style={{
          flex: 1,
          backgroundColor: COLORS.black,
        }}
      >
        {/*Header*/}
        <HeaderBar title="Profile" />

        {/* Details */}
        <ScrollView>
          {/* Email & User Id */}
          <View
            style={{
              flexDirection: "row",
              marginTop: SIZES.radius,
            }}
          >
            {/* Email and ID */}
            <View
              style={{
                flex: 1,
              }}
            >
              <Text
                style={{
                  colorL: COLORS.white,
                  ...FONTS.h3,
                }}
              >
                {dummyData.profile.email}
              </Text>
              <Text
                style={{
                  color: COLORS.lightGray3,
                  ...FONTS.body4,
                }}
              >
                ID: {dummyData.profile.id}
              </Text>
            </View>

            {/* Status */}
            <View
              style={{
                flexDirection: "row",
                alignItems: "center",
              }}
            >
              <Image
                source={icons.verified}
                style={{
                  height: 25,
                  width: 25,
                }}
              />
              <Text
                style={{
                  marginLeft: SIZES.base,
                  color: COLORS.lightGreen,
                  ...FONTS.body4,
                }}
              >
                Verified
              </Text>
            </View>
          </View>

          {/* APP */}

          <SectionTitle title="APP" />

          <Setting
            title="Launch Screen"
            value="Home"
            type="button"
            onPress={() => {}}
          />

          <Setting
            title="Appearance"
            value="Dark"
            type="button"
            onPress={() => {}}
          />

          {/* ACCOUNT */}
          <SectionTitle title="ACCOUNT" />

          <Setting
            title="Payment Currency"
            value="Home"
            type="button"
            onPress={() => {}}
          />

          <Setting
            title="Appearance"
            value="Dark"
            type="button"
            onPress={() => {}}
          />

          {/* SECURITY */}
          <SectionTitle title="SECURITY" />

          <Setting
            title="FaceID"
            value={faceId}
            type="switch"
            onPress={(value) => setFaceId(value)}
          />
          <Setting
            title="Appearance"
            value="Dark"
            type="button"
            onPress={() => {}}
          />
          <Setting
            title="Appearance"
            value="Dark"
            type="button"
            onPress={() => {}}
          />
        </ScrollView>
      </View>
    </MainLayout>
  );
};

export default Profile;
