import React, { useSelector, useDispatch, useState } from "react";
import { View, Text, FlatList, TouchableOpacity } from "react-native";
import { MainLayout } from "./MainLayout";
import { getHoldings, getCoinMarket } from "../store/market/marketActions";
import { useFocusEffect } from "@react-navigation/native";
import { SIZES, COLORS, FONTS, icons, holdings, dummyData } from "../constants";
import { BalanceInfo, IconTextButton, Chart } from "../components";

const Portfolio = () => {
  const myHoldings = useSelector((state) => state.myHoldings);
  const coins = useSelector((state) => state.coins);
  const dispatch1 = useDispatch(getHoldings);
  const dispatch2 = useDispatch(getCoinMarket);

  const [selectedCoin, setSelectedCoin] = useState(null);
  useFocusEffect(
    React.useCallback(() => {
      getHoldings((holdings = dummyData.holdings));
      getCoinMarket();
    }, [])
  );
  let totalWallet = myHoldings.reduce((a, b) => a + (b.total || 0), 0);
  let valueChange = myHoldings.reduce(
    (a, b) => a + (b.holding_value_change_7d || 0),
    0
  );
  let perChange = (valueChange / (totalWallet - valueChange)) * 100;
  function renderCurrentBalanceSection() {
    return (
      <View
        style={{
          paddingHorizontal: SIZES.padding,
          borderBottomLeftRadius: 25,
          borderBottomRightRadius: 25,
          backgroundColor: COLORS.gray,
        }}
      >
        <Text
          style={{
            marginTop: 50,
            color: COLORS.white,
            ...FONTS.largeTitle,
          }}
        >
          Portfolio
        </Text>
        <BalanceInfo
          title="Current Balance"
          displayAmount={totalWallet}
          changePct={perChange}
          containerStyle={{
            marginTop: SIZES.radius,
            marginBottom: SIZES.padding,
          }}
        />
      </View>
    );
  }
  return (
    <MainLayout>
      <View
        style={{
          flex: 1,
          backgroundColor: COLORS.black,
        }}
      >
        {/*Header*/}
        {renderCurrentBalanceSection()}

        {/*Chart */}
        <Char
          containerStyle={{
            marginTop: SIZES.radius,
          }}
          charPrices={
            selectedCoin
              ? selectedCoin?.sparkline_in_7d?.price
              : myHoldings[0]?.sparkline_in_7d?.price
          }
        />
        {/* Your Assets */}
        <FlatList
          data={myHoldings}
          keyExtractor={(item) => item.id}
          contentContainerStyle={{
            marginTop: SIZES.padding,
            paddingHorizontal: SIZES.padding,
          }}
          ListHeaderComponent={
            <View>
              <Text
                style={{
                  color: COLORS.white,
                  ...FONTS.h2,
                }}
              >
                Your Assets
                {/* Header label */}
                <View
                  style={{ flexDirection: "row", marginTop: SIZES.padding }}
                >
                  <Text
                    style={{
                      flex: 1,
                      color: COLORS.lightGray3,
                    }}
                  >
                    Asset
                  </Text>
                  <Text
                    style={{
                      flex: 1,
                      color: COLORS.lightGray3,
                      textAlign: "right",
                    }}
                  >
                    Price
                  </Text>
                  <Text
                    style={{
                      flex: 1,
                      color: COLORS.lightGray3,
                      textAlign: "right",
                    }}
                  >
                    Holdings
                  </Text>
                </View>
              </Text>
            </View>
          }
          renderItem={({ item }) => {
            let priceColor =
              item.price_change_percentage_7d_currency == 0
                ? COLORS.lightGray3
                : item.price_change_percentage_7d_currency > 0
                ? COLORS.lightGreen
                : COLORS.red;
            return (
              <TouchableOpacity
                style={{
                  height: 55,
                  flexDirection: "row",
                  height: 55,
                }}
                onPress={() => setSelectedCoin(item)}
              >
                {/* Asset */}
                <View
                  style={{
                    flex: 1,
                    flexDirection: "row",
                    alignItems: "center",
                  }}
                >
                  <Image
                    source={{ uri: item.image }}
                    style={{ width: 20, height: 20 }}
                  />
                  <Text
                    style={{
                      color: COLORS.white,
                      ...FONTS.h4,
                      marginLeft: SIZES.radius,
                    }}
                  >
                    {item.name}
                  </Text>
                </View>

                {/* Price */}
                <View style={{ flex: 1, justifyContent: "center" }}>
                  <Text
                    style={{
                      textAlign: "right",
                      color: COLORS.white,
                      ...FONTS.h4,
                      lineHeight: 15,
                    }}
                  >
                    ${item.current_price.toLocalString()}
                  </Text>
                  <View
                    style={{
                      flexDirection: "row",
                      alignItems: "center",
                      justifyContent: "flex-end",
                    }}
                  >
                    {item.price_change_percentage_7d_currency != 0 && (
                      <Image
                        source={icons.upArrow}
                        style={{
                          width: 10,
                          height: 10,
                          tintColor: priceColor,
                          transform:
                            item.price_change_percentage_7d_currency > 0
                              ? [{ rotate: "45deg" }]
                              : [{ rotate: "125deg" }],
                        }}
                      />
                    )}
                    <Text
                      style={{
                        marginLeft: 5,
                        color: priceColor,
                        ...FONTS.body5,
                        lineHeight: 15,
                      }}
                    >
                      {item.price_change_percentage_7d_currency.toFixed(2)}%
                    </Text>
                  </View>
                </View>

                {/* Holdings */}
                <View
                  style={{
                    flex: 1,
                    justifyContent: "center",
                  }}
                >
                  <Text
                    style={{
                      textAlign: "center",
                      color: COLORS.white,
                      ...FONTS.h4,
                      lineHeight: 15,
                    }}
                  >
                    ${item.total.toLocalString()}
                  </Text>

                  <Text
                    style={{
                      textAlign: "right",
                      color: COLORS.lightGray3,
                      ...FONTS.body5,
                      lineHeight: 15,
                    }}
                  >
                    {item.qty} {item.symbol.toUpperCase()}
                  </Text>
                </View>
              </TouchableOpacity>
            );
          }}
        />
      </View>
    </MainLayout>
  );
};

export default Portfolio;
