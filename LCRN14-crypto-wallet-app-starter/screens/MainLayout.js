import React, { useSelector } from "react";
import { View, Text, Animated } from "react-native";
import { FONTS, COLORS, SIZES, icons } from "../constants";
import { IconTextButton } from "../components";

const MainLayout = ({ children }) => {
  const isTrade = useSelector((state) => state.isTrade);
  const modalAnimatedValue = React.useRef(new Animated.Value(0)).current;
  React.useEffect(() => {
    if (isTrade) {
      Animated.timing(modalAnimatedValue, {
        toValue: 1,
        duration: 500,
        useNativeDriver: false,
      }).start();
    } else {
      Animated.timing(modalAnimatedValue, {
        toValue: 0,
        duration: 500,
        useNativeDriver: false,
      }).start();
    }
  }, [isTrade]);

  const modalY = modalAnimatedValue.interpolate({
    inputRange: [0, 1],
    outputRange: [SIZES.height, SIZES.height - 280],
  });
  return (
    <View
      style={{
        flex: 1,
      }}
    >
      {children}
      {/* Dim Brackgound */}
      {isTrade && (
        <Animated.View
          style={{
            position: "absolute",
            left: 0,
            top: 0,
            right: 0,
            bottom: 0,
            backgroundColor: COLORS.transparentBlack,
          }}
          opacity={modalAnimatedValue}
        />
      )}

      {/* Modal */}
      <Animated.View
        style={{
          position: "absolute",
          left: 0,
          top: modalY,
          with: "100%",
          padding: SIZES.padding,
          backgroundColor: COLORS.primary,
        }}
      >
        <IconTextButton label="Transfer" icon={icons.send} onPress={() => {}} />
        <IconTextButton
          label="Withdraw"
          icon={icons.withdraw}
          containerStyle={{
            marginTop: SIZES.base,
          }}
          onPress={() => {}}
        />
      </Animated.View>
    </View>
  );
};

export default MainLayout;
